using DelimitedFiles
using IonimageAnalysisCore
using Test

@testset "IonimageAnalysisCore.jl" begin
    @test IonimageAnalysisCore.crop(
        [1.  0.  0.  0.  0.
         0.  2.  0.  0.  0.
         0.  0.  3.  0.  0.
         0.  0.  0.  4.  0.
         0.  0.  0.  0.  5.], (2, 4, 1, 3)) ==
        [0.  2.  0.
         0.  0.  3.
         0.  0.  0.]
    @test IonimageAnalysisCore.crop(
        [1.  0.  0.  0.  0.
         0.  2.  0.  0.  0.
         0.  0.  3.  0.  0.
         0.  0.  0.  4.  0.
         0.  0.  0.  0.  5.], (1, 5, 1, 5)) ==
        [1.  0.  0.  0.  0.
         0.  2.  0.  0.  0.
         0.  0.  3.  0.  0.
         0.  0.  0.  4.  0.
         0.  0.  0.  0.  5.]
    @test IonimageAnalysisCore.crop(
        [1.  0.  0.  0.  0.
         0.  2.  0.  0.  0.
         0.  0.  3.  0.  0.
         0.  0.  0.  4.  0.
         0.  0.  0.  0.  5.], (2, 3), 3, 5) ==
        [1.  0.  0.  0.  0.
         0.  2.  0.  0.  0.
         0.  0.  3.  0.  0.]
    @test IonimageAnalysisCore.crop(
        [1.  0.  0.  0.  0.
         0.  2.  0.  0.  0.
         0.  0.  3.  0.  0.
         0.  0.  0.  4.  0.
         0.  0.  0.  0.  5.], (3, 2), 5, 3) ==
        [1.  0.  0.
         0.  2.  0.
         0.  0.  3.
         0.  0.  0.
         0.  0.  0.]
    @test IonimageAnalysisCore.crop(
        [1.  0.  0.  0.  0.
         0.  2.  0.  0.  0.
         0.  0.  3.  0.  0.
         0.  0.  0.  4.  0.
         0.  0.  0.  0.  5.], (2, 3), (3, 5)) ==
        [1.  0.  0.  0.  0.
         0.  2.  0.  0.  0.
         0.  0.  3.  0.  0.]
    @test IonimageAnalysisCore.crop(
        [1.  0.  0.  0.  0.
         0.  2.  0.  0.  0.
         0.  0.  3.  0.  0.
         0.  0.  0.  4.  0.
         0.  0.  0.  0.  5.], (3, 2), (5, 3)) ==
        [1.  0.  0.
         0.  2.  0.
         0.  0.  3.
         0.  0.  0.
         0.  0.  0.]
    @test IonimageAnalysisCore.crop(
        [1.  0.  0.  0.  0.
         0.  2.  0.  0.  0.
         0.  0.  3.  0.  0.
         0.  0.  0.  4.  0.
         0.  0.  0.  0.  5.], (2, 2), (5, 5)) ==
        [1.  0.  0.  0.
         0.  2.  0.  0.
         0.  0.  3.  0.
         0.  0.  0.  4.]
    @test IonimageAnalysisCore.crop(
        [1.  0.  0.  0.  0.
         0.  2.  0.  0.  0.
         0.  0.  3.  0.  0.
         0.  0.  0.  4.  0.
         0.  0.  0.  0.  5.], (2.5, 2.5), (4, 4)) ==
        [1.  0.  0.  0.
         0.  2.  0.  0.
         0.  0.  3.  0.
         0.  0.  0.  4.]

    @test IonimageAnalysisCore.shiftpos((1, 1),  2, 3) ==
          IonimageAnalysisCore.shiftpos((1, 1), (2, 3)) ==
          (3, 4)

    @test IonimageAnalysisCore.cornerpos(
        [1.  0.  0.  0.  0.
         0.  2.  0.  0.  0.
         0.  0.  3.  0.  0.
         0.  0.  0.  4.  0.
         0.  0.  0.  0.  5.], (3, 3), 3, 5) == (2, 4, 1, 5)
    @test IonimageAnalysisCore.cornerpos(
        [1.  0.  0.  0.  0.
         0.  2.  0.  0.  0.
         0.  0.  3.  0.  0.
         0.  0.  0.  4.  0.
         0.  0.  0.  0.  5.], (3, 3), 1) == (2, 4, 2, 4)
    @test IonimageAnalysisCore.cornerpos(
        [1.  0.  0.  0.  0.
         0.  2.  0.  0.  0.
         0.  0.  3.  0.  0.
         0.  0.  0.  4.  0.
         0.  0.  0.  0.  5.], (3, 3), 2) == (1, 5, 1, 5)

    @test IonimageAnalysisCore.maxradius(
        [1.  0.  0.  0.  0.
         0.  2.  0.  0.  0.
         0.  0.  3.  0.  0.
         0.  0.  0.  4.  0.
         0.  0.  0.  0.  5.], (3, 3)) == 2
    @test IonimageAnalysisCore.maxradius(
        [1.  0.  0.  0.  0.
         0.  2.  0.  0.  0.
         0.  0.  3.  0.  0.
         0.  0.  0.  4.  0.
         0.  0.  0.  0.  5.], (2, 3)) == 1
    @test IonimageAnalysisCore.maxradius(
        [1.  0.  0.  0.  0.
         0.  2.  0.  0.  0.
         0.  0.  3.  0.  0.
         0.  0.  0.  4.  0.
         0.  0.  0.  0.  5.], (4, 3)) == 1
    @test IonimageAnalysisCore.maxradius(
        [1.  0.  0.  0.  0.
         0.  2.  0.  0.  0.
         0.  0.  3.  0.  0.
         0.  0.  0.  4.  0.
         0.  0.  0.  0.  5.], (3, 2)) == 1
    @test IonimageAnalysisCore.maxradius(
        [1.  0.  0.  0.  0.
         0.  2.  0.  0.  0.
         0.  0.  3.  0.  0.
         0.  0.  0.  4.  0.
         0.  0.  0.  0.  5.], (3, 4)) == 1

    @test IonimageAnalysisCore.circumscribedsquare(
        [1.  0.  0.  0.  0.
         0.  2.  0.  0.  0.
         0.  0.  3.  0.  0.
         0.  0.  0.  4.  0.
         0.  0.  0.  0.  5.], (3, 3), 2) ==
       ([1.  0.  0.  0.  0.
         0.  2.  0.  0.  0.
         0.  0.  3.  0.  0.
         0.  0.  0.  4.  0.
         0.  0.  0.  0.  5.], (3., 3.), 2.)
    @test IonimageAnalysisCore.circumscribedsquare(
        [1.  0.  0.  0.  0.
         0.  2.  0.  0.  0.
         0.  0.  3.  0.  0.
         0.  0.  0.  4.  0.
         0.  0.  0.  0.  5.], (3, 3), 1) ==
       ([2.  0.  0.
         0.  3.  0.
         0.  0.  4.], (2., 2.), 1.)
    @test IonimageAnalysisCore.circumscribedsquare(
        [1.  0.  0.  0.  0.
         0.  2.  0.  0.  0.
         0.  0.  3.  0.  0.
         0.  0.  0.  4.  0.
         0.  0.  0.  0.  5.], (3, 3), 3) ==
       ([1.  0.  0.  0.  0.
         0.  2.  0.  0.  0.
         0.  0.  3.  0.  0.
         0.  0.  0.  4.  0.
         0.  0.  0.  0.  5.], (3., 3.), 2.)
    @test IonimageAnalysisCore.circumscribedsquare(
        [1.  0.  0.  0.  0.
         0.  2.  0.  0.  0.
         0.  0.  3.  0.  0.
         0.  0.  0.  4.  0.
         0.  0.  0.  0.  5.], (2, 3), 1) ==
       ([0.  0.  0.
         2.  0.  0.
         0.  3.  0.], (2., 2.), 1.)
    @test IonimageAnalysisCore.circumscribedsquare(
        [1.  0.  0.  0.  0.
         0.  2.  0.  0.  0.
         0.  0.  3.  0.  0.
         0.  0.  0.  4.  0.
         0.  0.  0.  0.  5.], (3, 3), 1.5) ==
       ([1.  0.  0.  0.  0.
         0.  2.  0.  0.  0.
         0.  0.  3.  0.  0.
         0.  0.  0.  4.  0.
         0.  0.  0.  0.  5.], (3., 3.), 2.)
    @test IonimageAnalysisCore.circumscribedsquare(
        [1.  0.  0.  0.  0.
         0.  2.  0.  0.  0.
         0.  0.  3.  0.  0.
         0.  0.  0.  4.  0.
         0.  0.  0.  0.  5.], (2.5, 2.5), 1.5) ==
       ([1.  0.  0.  0.
         0.  2.  0.  0.
         0.  0.  3.  0.
         0.  0.  0.  4.], (2.5, 2.5), 1.5)

    @test pixel_to_speed(100, 10.0e3, 10.0e-6, 1.0) ≈ 1000.
    @test pixel_to_speed(100, 10.0e3, 10.0e-6, 2.0) ≈ 500.

    @test speed_to_energy(1000., 16, 32) ≈ 16.
    @test speed_to_energy( 700., 15, 50) ≈ 5.25

    @test pixel_to_energy(100, 16, 32, 10.0e3, 10.0e-6, 1.0) ≈ 16.
    @test pixel_to_energy(100, 16, 32, 10.0e3, 10.0e-6, 2.0) ≈ 4.

    @test energy_to_speed(16., 16, 32) ≈ 1000.
    @test energy_to_speed(5.25, 15, 50) ≈  700.

    let θ = collect(0:1:180)
        P₂(θ) = 1/2*(3*cosd(θ)^2 - 1)
        P₄(θ) = 1/8*(35*cosd(θ)^4 - 30*cosd(θ)^2 + 3)
        @test all(angularfit(θ, [ 2., 1.], [2]) .≈ 1 .+ 2 .* P₂.(θ))
        @test all(angularfit(θ, [ 0., 1.], [2]) .≈ 1)
        @test all(angularfit(θ, [-1., 1.], [2]) .≈ 1 .+ (-1) .* P₂.(θ))
        @test all(angularfit(θ, [ 2., 1., 1.], [2, 4]) .≈ 1 .+ 2 .* P₂.(θ) .+ 1 .* P₄.(θ))
    end

    let
        v, Iv = speeddist([1., 2., 3.], [2., 4., 6.], 10e3, 10e-6, 1.)
        @test all(v  .≈ [10., 20., 30.])
        @test all(Iv .≈ [2., 4., 6.])
    end
    let
        v, Iv = speeddist([1., 2., 3.], [2., 4., 6.], 10e3, 10e-6, 2.)
        @test all(v  .≈ [5., 10., 15.])
        @test all(Iv .≈ [2., 4., 6.])
    end

    let
        Et, IEt = energydist([1., 2., 3.], [3., 6., 9.], 16, 32)
        @test all(Et  .≈ [16e-6, 64e-6, 144e-6])
        @test all(IEt .≈ [0.09375, 0.09375, 0.09375])
    end
    let
        Et, IEt = energydist([14., 28., 42.], [3., 6., 9.], 15, 50)
        @test all(Et  .≈ [2.1e-3, 8.4e-3, 18.9e-3])
        @test all(IEt .≈ [0.01, 0.01, 0.01])
    end

    @test average([1., 2., 3.], [1, 1, 1]) ≈ 2.
    @test average([1., 2., 3.], [1, 1, 2]) ≈ 2.25

    let
        img = rand(Float64, (100, 100))
        smoothed = smooth(img, 3)
        @test minimum(img) ≤ minimum(smoothed)
        @test maximum(smoothed) ≤ maximum(img)

        kernel = [1.  2.  1.
                  2.  4.  2.
                  1.  2.  1.]
        kernel ./= sum(kernel)
        smoothed = smooth(img, kernel)
        @test minimum(img) ≤ minimum(smoothed)
        @test maximum(smoothed) ≤ maximum(img)
    end

    let data = readdlm(joinpath(@__DIR__, "radialdist.tsv"))
        r  = data[:, 1]
        Ir = data[:, 2]

        v, Iv = speeddist(r, Ir, 10e3, 10e-6, 2.0)
        @test 175. < v[argmax(Iv)] < 225.
        @test maximum(Iv[1:35])   ≈ 0.
        @test maximum(Iv[45:end]) ≈ 0.

        Et, IEt = energydist(v, Iv, 16, 32)
        @test 0.50 < Et[argmax(IEt)] < 0.81
        @test maximum(IEt[1:35])   ≈ 0.
        @test maximum(IEt[45:end]) ≈ 0.
    end

    @test @isdefined radialdist
    @test @isdefined anisotropy
end
