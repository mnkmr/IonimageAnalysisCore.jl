module IonimageAnalysisCore

export
    pixel_to_speed,
    speed_to_energy,
    pixel_to_energy,
    energy_to_speed,
    angularfit,
    average,
    smooth,
    radialdist,
    speeddist,
    energydist,
    anisotropy

using ImageFiltering: imfilter, centered


"""
    newimage = crop(image, corners)

Crop image with the corners (ystart, yend, xstart, xend).
    newimg == image[ystart, yend, xstart, xend]
"""
function crop(image, corners)
    ymin, ymax, xmin, xmax = corners
    image[ymin:ymax, xmin:xmax]
end

"""
    newimage = crop(image, center, height, width)
    newimage = crop(image, center, (height, width))

Crop image around the center (yc, xc) with the size (height, width).

!!! info "Schematic of geometry"
```
-----------------------------------------------------
| img                                               |
|                     width                         |
|               -----------------                   |
|               | newimage      |                   |
|               |               |                   |
|        height |       *       |                   |
|               |     center    |                   |
|               |    (yc, xc)   |                   |
|               -----------------                   |
|                                                   |
|                                                   |
-----------------------------------------------------
```
"""
crop(image, center, height, width) = crop(image, cornerpos(image, center, height, width))
crop(image, center, size) = crop(image, center, size...)


"Process shear mapping for an image and return the new image"
function shear(image, θ)
    height, width = size(image)
    yc, xc = ceil.(Int, [height, width] ./ 2)
    newimg = zeros(Float64, (height, width))
    for x in 1:width, y in 1:height
        if y == yc
            newimg[y, x] += image[y, x]
            continue
        end
        xnew = round(Int, (y-yc)*tand(θ) + x)
        if 1 <= xnew <= width
            newimg[y, xnew] += image[y, x]
        end
    end
    newimg
end


"Move a position in a parallel direction"
shiftpos(pos0, dy, dx) = pos0[1] + dy, pos0[2] + dx
shiftpos(pos0, delta) = shiftpos(pos0, delta[1], delta[2])


"Compute the four corner coordinates to crop a `height` x `width` image with the `center`"
function cornerpos(image, center, height, width)
    Ny, Nx = size(image)
    edge(xc, d, xmax) = max(floor(Int, xc - d), 1), min(floor(Int, xc + d), xmax)
    ymin, ymax = edge(center[1], (height - 1)/2, Ny)
    xmin, xmax = edge(center[2], (width - 1)/2, Nx)
    ymin, ymax, xmin, xmax
end
function cornerpos(image, center, rmax)
    Ny, Nx = size(image)
    edge(xc, r, xmax) = max(floor(Int, xc - r), 1), min(ceil(Int, xc + r), xmax)
    ymin, ymax = edge(center[1], rmax, Ny)
    xmin, xmax = edge(center[2], rmax, Nx)
    ymin, ymax, xmin, xmax
end



"Return the maximum radius included in the image with the center"
function maxradius(img, center)
    yc, xc = center
    ny, nx = size(img)
    !(1 <= yc <= ny) && return 0
    !(1 <= xc <= nx) && return 0
    float(min(yc - 1, xc - 1, ny - yc, nx - xc))
end


"Return the circumscribed square and its original corner coordinates (ymin, ymax, xmin, xmax)"
function circumscribedsquare(image0, center0, rmax)
    ymin, _, xmin, _ = corners = cornerpos(image0, center0, rmax)
    image = crop(image0, corners)
    center = shiftpos(center0, -ymin + 1.0, -xmin + 1.0)
    rmax = maxradius(image, center)
    image, center, rmax
end


"""
    v = pixel_to_speed(r, ppm, tof, N)

Translate pixel distance to fragment speed.

See also: [`speeddist`](@ref), [`pixel_to_energy`](@ref)

# Arguments
- `r`  : the vector of radii in pixel.
- `ppm`: pixel-per-meter (pixel/m).
- `tof`: time-of-flight in second.
- `N`  : a magnification factor of electrostatic optics.[1]

# Example
```julia-repl
julia> using IonimageAnalysisCore

julia> ppm = 20e3; # 20 pixel/mm = 20e3 pixel/m

julia> tof = 10.0e-6; # 10.0 μs = 10.0e-6 s

julia> v = pixel_to_speed(1:100, ppm, tof, 1.);
```

[1] Eppink, A. T. J. B.; Parker, D. H. Velocity Map Imaging of Ions and
    Electrons Using Electrostatic Lenses: Application in Photoelectron and
    Photofragment Ion Imaging of Molecular Oxygen. Rev. Sci. Instrum. 1997, 68
    (9), 3477.
"""
pixel_to_speed(r, ppm, tof, N) = r / N / ppm / tof


"""
    Et = speed_to_energy(v, m, M)

Translate fragment speed to center-of-mass kinetic energy release in kJ/mol.

See also: [`energy_to_speed`](@ref), [`speeddist`](@ref), [`energydist`](@ref)

# Arguments
- `v`: the fragment recoil speed in m/s.
- `m`: the mass of the fragment in atomic unit.
- `M`: the mass of the parent molecule in atomic unit.

# Example
```julia-repl
julia> using IonimageAnalysisCore

julia> ppm = 20e3; # 20 pixel/mm = 20e3 pixel/m

julia> tof = 10.0e-6; # 10.0 μs = 10.0e-6 s

julia> v = pixel_to_speed(1:100, ppm, tof);

julia> m, M = 127, 142; # For example, observing I⁺ of CH₃I -> CH₃ + I

julia> Et = speed_to_energy(v, m, M);  # in kJ/mol
```
"""
speed_to_energy(v, m, M) = 0.5*(m*1e-3)*v*v*(M/(M - m))*1e-3


"""
    pixel_to_energy(r, m, M, ppm, tof, N)

Translate pixel distance to center-of-mass kinetic energy release in kJ/mol.

See also: [`pixel_to_speed`](@ref)

# Arguments
- `r`  : the vector of radii in pixel.
- `m`  : the mass of the fragment in atomic unit.
- `M`  : the mass of the parent molecule in atomic unit.
- `ppm`: pixel-per-meter (pixel/m).
- `tof`: time-of-flight in second.
- `N`  : a magnification factor of electrostatic optics.
"""
pixel_to_energy(r, m, M, ppm, tof, N) =
    speed_to_energy(pixel_to_speed(r, ppm, tof, N), m, M)


"""
    v = energy_to_speed(Et, m, M)

Translate center-of-mass kinetic energy release to fragment speed.

See also: [`speed_to_energy`](@ref), [`speeddist`](@ref), [`energydist`](@ref)

# Arguments
- `Et`: the kinetic energy release in kJ/mol.
- `m` : the mass of the fragment in atomic unit.
- `M` : the mass of the parent molecule in atomic unit.

# Example
```julia-repl
julia> using IonimageAnalysisCore

julia> Et, IEt = energydist(imagedata); # imagedata is provided by another package

julia> m, M = 127, 142; # For example, observing I⁺ of CH₃I -> CH₃ + I

julia> v = energy_to_speed(Et, m, M);
```
"""
function energy_to_speed(Et, m, M)
    M = M*1e-3  # kg/mol
    m = m*1e-3  # kg/mol
    sqrt(2(M - m)/(M*m)*Et*1e3)
end


"Calculate Legendre polynomials Pₗ(x)"
function legendre(l::Integer, x::Real)::Float64
    if l == 0
        return Float64(1)
    elseif l == 1
        return Float64(x)
    elseif l == 2
        return (3*x^2 - 1)/2
    elseif l == 3
        return (5*x^3 - 3*x)/2
    elseif l == 4
        return (35*x^4 - 30*x^2 + 3)/8
    elseif l == 5
        return (63*x^5 - 70*x^3 + 15*x)/8
    elseif l == 6
        return (231*x^6 - 315*x^4 + 105*x^2 - 5)/16
    elseif l == 7
        return (429*x^7 - 693*x^5 + 315*x^3 - 35*x)/16
    elseif l == 8
        return (6435*x^8 - 12012*x^6 + 6930*x^4 - 1260*x^2 + 35)/128
    end

    Pₙ = legendre(7, x)
    Pₙ₊₁ = legendre(8, x)
    for n in 8:(l - 1)
        Pₙ₋₁ = Pₙ
        Pₙ = Pₙ₊₁
        Pₙ₊₁ = ((2n + 1)*x*Pₙ - n*Pₙ₋₁)/(n + 1)
    end
    Pₙ₊₁
end


"""
    angularfit(θ, b, l)

Compute Zare's equation of photofragment angular dissociation.
If `l = [2, 4]`, it calculates
    `1 .+ b[1].*P₂(cosθ) .+ b[2].*P₄(cosθ)`.
θ is a vector of angles in degree and Pₙ(cosθ) are n-th order term of Legendre
polynomials.
"""
function angularfit(θ, b, l)
    ysim = @. float(one(θ))
    for (i, l) in enumerate(l)
        β = b[i]
        @. ysim += β*legendre(l, cosd(θ))
    end
    ysim
end


"""
    average(x, fx)

Calculate the weighted average `⟨x⟩` of `x` with its distribution function
`fx`.

# Examples
```julia-repl
julia> using IonimageAnalysisCore

julia> ppm = 20e3; # 20 pixel/mm = 20e3 pixel/m

julia> tof = 10.0e-6; # 10.0 μs = 10.0e-6 s

julia> v, Iv = speeddist(imagedata);

julia> v_ave = average(v, Iv);  # obtain ⟨v⟩

julia> m, M = 127, 142; # For example, observing I⁺ of CH₃I -> CH₃ + I

julia> Et, IEt = energydist(v, Iv, m, M)

julia> Et_ave = average(Et, IEt); # obtain ⟨Et⟩
```
"""
function average(x, w)
    sumwx = zero(eltype(x))
    sumw = zero(eltype(w))
    for (x, w) in zip(x, w)
        sumwx += x*w
        sumw += w
    end
    sumwx / sumw
end


"""
    smooth(img::AbstractMatrix{<:Real}, kernel::AbstractMatrix{<:Real}, args...)

Smooth the image with the assigned kernel matrix.
"""
smooth(img::AbstractMatrix{<:Real}, kernel::AbstractMatrix{<:Real}, args...) = imfilter(img, centered(kernel), args...)

"""
    smooth(img::AbstractMatrix{<:Real}, edge::Integer, args...)

Smooth the image with an average filter kernel. `edge` is the length of average
filter kernel; it should be a positive integer.

# Examples
```julia-repl
julia> using IonimageAnalysisCore, PyPlot

julia> image = rand(Float64, (100, 100));

julia> imshow(image);

julia> smoothed = smooth(image, 3);  # smooth with 3x3 average filter kernel

julia> imshow(smoothed);
```
"""
function smooth(img::AbstractMatrix{<:Real}, edge::Integer, args...)
    average_filter_kernel = ones(Float64, (edge, edge))
    average_filter_kernel ./= sum(average_filter_kernel)
    smooth(img, average_filter_kernel, args...)
end


############################## Interfaces ##############################
#  The functions listed below aim to provide common interfaces for image
# analysis. The actual methods will be defined by external packages.
########################################################################

"""
    r, Ir = radialdist(imagedata)

Compute the radial distribution of `imagedata`. The `imagedata` will be
provided by an external package.

# Outputs
- `r` : the vector of radii in pixel.
- `Ir`: the corresponding radial distribution.

# Example
```julia-repl
julia> using IonimageAnalysisCore

julia> r, Ir = radialdist(imagedata)
```
"""
function radialdist end


"""
    v, Iv = speeddist(imagedata, ppm, tof, N)

Compute the speed distribution of `imagedata`. The `imagedata` will be
provided by an external package.

See also: [`pixel_to_speed`](@ref)

# Outputs
- `v`: the vector of fragment recoil speed in m/s.
- `Iv`: the corresponding speed distribution.

# Arguments
- `imagedata`: image data provided by external packages in various form.
- `ppm`: pixel-per-meter (pixel/m).
- `tof`: time-of-flight in second.
- `N`: a magnification factor of electrostatic optics.[1]

# Example
```julia-repl
julia> using IonimageAnalysisCore

julia> ppm = 20e3; # 20 pixel/mm = 20e3 pixel/m

julia> tof = 10.0e-6; # 10.0 μs = 10.0e-6 s

julia> v, Iv = speeddist(imagedata, ppm, tof, 1.); # imagedata is provided by another package
```

[1] Eppink, A. T. J. B.; Parker, D. H. Velocity Map Imaging of Ions and
    Electrons Using Electrostatic Lenses: Application in Photoelectron and
    Photofragment Ion Imaging of Molecular Oxygen. Rev. Sci. Instrum. 1997, 68
    (9), 3477.
"""
function speeddist end

"""
    v, Iv = speeddist(r, Ir, ppm, tof, N)

Calculate a speed distribution from a corresponding radial distribution.

# Arguments
- `r`: the vector of radii in pixel.
- `ppm`: pixel-per-meter (pixel/m).
- `tof`: time-of-flight in second.
- `N`: a magnification factor of electrostatic optics.

# Example
```julia-repl
julia> using IonimageAnalysisCore

julia> r = 1:100;

julia> Iv = exp.(-(r .- 50).^2 ./ (2*5^2));

julia> ppm = 20e3; # 20 pixel/mm = 20e3 pixel/m

julia> tof = 10.0e-6; # 10.0 μs = 10.0e-6 s

julia> v, Iv = speeddist(r, Iv, ppm, tof, 1.);
```
"""
function speeddist(r, Ir, ppm, tof, N)
    v = pixel_to_speed.(r, ppm, tof, N)
    Iv = Ir
    v, Iv
end


"""
    Et, IEt = energydist(imagedata, ppm, tof, N, m, M)

Coumpute the energy distribution of `imagedata`. The `imagedata` will be
provided by an external package.

See also: [`speeddist`](@ref), [`speed_to_energy`](@ref), [`energy_to_speed`](@ref)

# Outputs
- `Et`: the kinetic energy release in kJ/mol.
- `IEt`: the corresponding energy distribution.

# Arguments
- `imagedata`: image data provided by external packages in various form.
- `ppm`: pixel-per-meter (pixel/m).
- `tof`: time-of-flight in second.
- `N`: a magnification factor of electrostatic optics. ([`speeddist`](@ref))
- `m`: the mass of the fragment in atomic unit.
- `M`: the mass of the parent molecule in atomic unit.

# Example
```julia-repl
julia> using IonimageAnalysisCore

julia> m, M = 127, 142; # For example, observing I⁺ of CH₃I -> CH₃ + I

julia> ppm = 20e3; # 20 pixel/mm = 20e3 pixel/m

julia> tof = 10.0e-6; # 10.0 μs = 10.0e-6 s

julia> Et, IEt = energydist(imagedata, ppm, tof, 1., m, M); # imagedata is provided by another package
```
"""
function energydist end

"""
    Et, IEt = energydist(v, Iv, m, M)

Calculate a energy distribution from a corresponding speed distribution.

# Arguments
- `v` : the vector of fragment recoil speed in m/s.
- `Iv`: the corresponding speed distribution.
- `m` : the mass of the fragment in atomic unit.
- `M` : the mass of the parent molecule in atomic unit.

# Example
```julia-repl
julia> using IonimageAnalysisCore

julia> r = 1:100;

julia> Ir = exp.(-(r .- 50).^2 ./ (2*5^2));

julia> ppm = 20e3; # 20 pixel/mm = 20e3 pixel/m

julia> tof = 10.0e-6; # 10.0 μs = 10.0e-6 s

julia> v, Iv = speeddist(r, Ir, ppm, tof);

julia> m, M = 127, 142; # For example, observing I⁺ of CH₃I -> CH₃ + I

julia> Et, IEt = energydist(v, Iv, m, M)
```
"""
function energydist(v, Iv, m, M)
    Et  = @. speed_to_energy(v, m, M)
    IEt = @. Iv*(M - m)/(M*m*v)
    Et, IEt
end


"""
    r, b, σb, l = anisotropy(imagedata; l=[2])

Compute the anisotropy parameters of `imagedata`. The `imagedata` will be
provided by an external package.

# Outputs
- `r` : the vector of radii in pixel.
- `b` : the matrix of fitting parameters.
- `σb`: the matrix of standard error of `b`.
- `l` : the vector of orders of Legendre polynomials to fit angular distributions.

# Argument `l`
`l` is a vector of orders of Legendre polynomials to take into account for
angular fitting. The items in `l` should be integer larger than zero (1, 2, ...)
If `l = [2]`, the angular distribution is fitted by
    I(θ) = B(1 + β₂*P₂(cosθ)).
The resulted `b` is a `length(r)`×`length(l)` matrix; therefore, `b[:, 1]` is
the vector of `β₂`. If `l` includes additional terms, the columns of `b` will
be increased. In case `l = [2, 4]`, the angular distribution is fitted by
    I(θ) = B(1 + β₂*P₂(cosθ) + β₄*P₄(cosθ)).
The first and second columns of `b` are the vectors of `β₂` and `β₄`,
respectively. The corresponding order number is shown in the returned `l`.
`B` is the intensity factor and thus it is proportional to the
radialdistribution `Ir`. See [`radialdist`](@ref).

# Example
```julia-repl
julia> using IonimageAnalysisCore, PyPlot

julia> r, b, σb = anisotropy(imagedata); # imagedata is provided by another package

julia> β₂, σβ₂ = b[:, 1], σb[:, 1];

julia> errorbar(r, β₂, σβ₂)
```
"""
function anisotropy end


end # module
